var mongoose = require('mongoose');
var express = require('express');
var passport = require('passport');
var router = express.Router();
var User = mongoose.model('User');
var Step = mongoose.model('Step');
var Beat = mongoose.model('Beat');
var jwt = require('express-jwt');
var auth = jwt({secret: 'STRONG', userProperty: 'payload'});

/****************************************************/
/* REGISTER AND LOGIN */
/****************************************************/
// register for a user account
router.post('/register', function(req, res, next) {
    if(!req.body.username || !req.body.password) {
        return res.status(400).json({message: 'Please fill out all fields.'});
    }

    var user = new User();
    user.username = req.body.username;
    user.setPassword(req.body.password);

    user.save(function (err) {
        if(err){ return res.status(400).json({message: 'Username taken. Pick a different one.'}); }
        return res.json({token: user.generateJWT()});
    });
});

// logging in
router.post('/login', function(req, res, next) {
    if(!req.body.username || !req.body.password) {
        return res.status(400).json({message: 'Please fill out all fields.'});
    }

    passport.authenticate('local-login', function(err, user, info) {
        if(err) { return next(err); }

        if(user) {
            return res.json({token: user.generateJWT()});
        } else {
            return res.status(401).json(info);
        }
    })(req, res, next);
});

/****************************************************/
/* PARAMETERS */
/****************************************************/
// setting up a profile parameter
router.param('profile', function(req, res, next, id){
    var q = User.findById(id);

    q.exec(function (err, profile){
        if(err){ return next(err);}

        if(!profile){
            return next(new Error ("Can't find Profile"));
        }

        req.profile =  profile;
        return next();
    });
});

// setting up step parameter
router.param('walk', function (req, res, next, id) {
    var q = Step.findById(id);

    q.exec(function (err, walk) {
        if(err){ return next(err);}

        if(!walk){
            return next(new Error("Can't find walk"));
        }

        req.walk = walk;
        return next();
    });
});

// setting up a heart parameter
router.param('heart', function(req, res, next, id){
    var q = Beat.findById(id);

    q.exec(function (err, heart){
        if(err){ return next(err);}

        if(!heart){
            return next(new Error ("Can't find Beat"));
        }

        req.heart =  heart;
        return next();
    });
});

/****************************************************/
/* STEPS */
/****************************************************/
// posting steps data
router.post('/user/:profile/run', function (req, res, next) {
    var walk = new Step (req.body);
    walk.user = req.profile;

    walk.save(function (err, walk) {
        if(err){ return res.status(500).json({message: 'Date already exists! Use Search bar to look up and edit/delete.(Click to remove message.)'});}

        req.profile.stepz.push(walk);

        req.profile.save(function (err, profile) {
            if(err){ return next (err);}

            res.json(walk);
        });
    });
});

// get a user's steps
router.get('/user/:profile/run', function (req, res) {
    req.profile.populate('stepz', function (err, profile) {
        res.json(profile.stepz);
    });
});

// delete a step row
router.delete('/user/:profile/run/:walk', function (req, res) {
    Step.remove({
        _id: req.params.walk
        },
        function(err, walk){
            if(err){return next(err);}

            req.profile.populate('stepz', function (err, profile) {
                res.json(profile.stepz);
            });
    });
});

// update steps
router.put('/user/:profile/run/:walk',function(req, res, next){
    Step.findById(req.params.walk, function(err, walk){
        if(err){return next(err);}

        walk.user = req.body.profile;
        walk.date = req.body.date;
        walk.step = req.body.step;

        walk.save(function (err, walk) {
            if(err){ return res.status(500).json({message: 'Date already exists! Use Search bar to look up and edit/delete.(Click to remove message.)'});}

            req.profile.save(function (err, profile) {
                if(err){ return next (err);}

                res.json(walk);
            });

            req.profile.populate('stepz', function (err, profile) {
                res.json(profile.stepz);
            });
        });
    })
});

/****************************************************/
/* HEARTBEATS */
/****************************************************/
// posting beats data
router.post('/user/:profile/heartbeat', function (req, res, next) {
    var heart = new Beat (req.body);
    heart.user = req.profile;

    heart.save(function (err, heart) {
        if(err){ return res.status(500).json({message: 'Date already exists! Use Search bar to look up and edit/delete.(Click to remove message.)'});}

        req.profile.beatz.push(heart);

        req.profile.save(function (err, profile) {
            if(err){ return next (err);}

            res.json(heart);
        });
    });
});

// get a user's beats
router.get('/user/:profile/heartbeat', function (req, res) {
    req.profile.populate('beatz', function (err, profile) {
        res.json(profile.beatz);
    });
});

// delete a heartbeat row
router.delete('/user/:profile/heartbeat/:heart', function (req, res) {
    Beat.remove({
            _id: req.params.heart
        },
        function(err, heart){
            if(err){return next(err);}

            req.profile.populate('beatz', function (err, profile) {
                res.json(profile.beatz);
            });
    });
});

// update a set of beats
router.put('/user/:profile/heartbeat/:heart',function(req,res, next){
    Beat.findById(req.params.heart, function(err, heart){
        if(err){return next(err);}

        heart.user = req.body.profile;
        heart.date = req.body.date;
        heart.min = req.body.min;
        heart.max = req.body.max;
        heart.avg = req.body.avg;

        heart.save(function (err, heart) {
            if(err){ return res.status(500).json({message: 'Date already exists! Use Search bar to look up and edit/delete.(Click to remove message.)'});}

            req.profile.save(function (err, profile) {
                if(err){ return next (err);}
                res.json(heart);
            });

            req.profile.populate('beatz', function (err, profile) {
                res.json(profile.beatz);
            });
        });
    })
});

//************** GET HOME PAGE *******************//
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

module.exports = router;
