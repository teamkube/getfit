/*
 *
 * Author : Prasanna Shiwakoti
 * Modified : 12/13/2016
 *
 * Class :
 *      Beat
 *
 * Attributes :
 *      date -> date
 *      max -> maximum heartbeat
 *      min -> minimum heartbeat
 *      avg -> average heartbeat
 *      user -> user's ID
 *
 * Methods :
 *      $scope.addBeat ->  takes the user's min, max, and avg and passes the Beat object with user's id to createBeat in
 *                          heartbeatService
 *
 *      $scope.deleteOneBeat -> deletes a Beat's row by passing the Beat object to deleteBeat in heartbeatService
 *          params:
 *              beat -> Beat object
 *
 *      $scope.startBeatEdit -> starts Beat row's edit
 *           params:
 *              beat -> Beat object
 *
 *      $scope.stopBeatEdit -> stops Beat row's edit
 *           params:
 *              beat -> Beat object
 *
 *      $scope.sendBeatEdit -> takes the user's min, max, and avg and if any of it has been changed passes
 *                              the Beat object with user's id to updateBeat in heartbeatService
 *          params:
 *              beat -> Beat object
 *              date -> date
 *              min -> Beat object's min
 *              max -> Beat object's max
 *              avg -> Beat object's avg
 *
 *      $scope.sortDate -> sets $Scope.sortStyle to Beat.date
 *
 *      $scope.sortMin -> sets $Scope.sortStyle to Beat.max
 *
 *      $scope.sortMax -> sets $Scope.sortStyle to Beat.min
 *
 *      $scope.sortAvg -> sets $Scope.sortStyle to Beat.avg
 *
 *      $scope.remErr -> set $scope.error to an empty string
 *
 *      $scope.maxChart -> sets $Scope.chartKind to Beat.max
 *
 *      $scope.minChart -> sets $Scope.chartKind to Beat.min
 *
 *      $scope.avgChart -> sets $Scope.chartKind to Beat.avg
 *
 *      $scope.makeChart -> makes the chart using Google's charts library and drawChart method
 *
 */

app.controller('HeartbeatController', [
    '$scope', 'allBeats','auth',
    function($scope, allBeats, auth){
        $scope.allBeats = allBeats.allBeats;
        $scope.profile = auth.currentUserId();
        $scope.date = new Date();
        $scope.maxDate = new Date();

        // variables used for search function
        $scope.sortStyle = 'date';
        $scope.sortBeat = '';
        $scope.flipSort = false;

        // variables used for graph function
        $scope.beginDate = '';
        $scope.lastDate = '';
        var chartKind = 'Maximum';
        var recommended = 170;

        // adding a heartbeat
        $scope.addBeat = function () {
            if(!$scope.date || $scope.date === '' || !$scope.min || $scope.min === ''
                || !$scope.max || $scope.max === '' || !$scope.avg || $scope.avg === ''){
                return ;
            };

            if($scope.max < 40 || $scope.min < 40 || $scope.max < 40 || $scope.max > 200 || $scope.min > 200 || $scope.avg > 200){
                $scope.error = new Error("All heartbeat must be between 40 and 200!(Click to remove message.)");
                return ;
            };

            if(!($scope.min <= $scope.avg && $scope.avg <= $scope.max)){
                $scope.error = new Error("Minimum must be smaller or equal to Average and Average must be smaller or equal to Maximum. Check your data!(Click to remove message.)");
                return;
            };

            $scope.date.setHours(0,0,0,0);

            allBeats.createBeat({
                date: $scope.date,
                min: $scope.min,
                max: $scope.max,
                avg: $scope.avg,
                user: $scope.profile
            }, auth.currentUserId())
                .error(function(error){
                $scope.error = error;
            });

            $scope.min = '';
            $scope.max = '';
            $scope.avg = '';
            $scope.editing = 'false';
        };

        // deleting a heartbeat
        $scope.deleteOneBeat = function (beat) {
            allBeats.deleteBeat(beat, auth.currentUserId());
        };

        // starting heartbeat row edit
        $scope.startBeatEdit = function (beat) {
            beat.editing = true;
        };

        // stopping heartbeat row edit
        $scope.stopBeatEdit = function (beat) {
            beat.editing = false;
        };

        // stopping beat row edit
        $scope.sendBeatEdit = function (beat, date, min, max, avg) {
            beat.editing = false;
            var tempDate = beat.date;
            var tempMin = beat.min;
            var tempMax = beat.max;
            var tempAvg = beat.avg;

            if(max < 40 || min < 40 || max < 40 || max > 200 || min > 200 || avg > 200){
                $scope.error = new Error("All heartbeat must be between 40 and 200!(Click to remove message.)");
                return ;
            };

            if(!(min <= avg && avg <= max)){
                $scope.error = new Error("Minimum must be smaller or equal to Average and Average must be smaller or equal to Maximum. Check your data!(Click to remove message.)");
                return;
            };

            beat.date = date;
            beat.min = min;
            beat.max = max;
            beat.avg = avg;
            allBeats.updateBeat(beat, auth.currentUserId())
                .error(function(error){
                $scope.error = error;
                beat.date = tempDate;
                beat.min = tempMin;
                beat.max = tempMax;
                beat.avg = tempAvg;
            });
        };

        // sorting by desired value
        $scope.sortDate = function(){
            $scope.flipSort = (!($scope.flipSort));
            $scope.sortStyle = 'date';
        };

        $scope.sortMin = function(){
            $scope.flipSort = (!($scope.flipSort));
            $scope.sortStyle = 'min';
        };

        $scope.sortMax = function(){
            $scope.sortStyle = 'max';
            $scope.flipSort = (!($scope.flipSort));
        };

        $scope.sortAvg = function(){
            $scope.flipSort = (!($scope.flipSort));
            $scope.sortStyle = 'avg';
        };

        $scope.remErr = function () {
            $scope.error = "";
        };

        // graph and set recommended by desired value
        $scope.maxChart = function () {
            chartKind = 'Maximum';
            recommended = 170;
        }

        $scope.minChart = function () {
            chartKind = 'Minimum';
            recommended = 60;
        }

        $scope.avgChart = function () {
            chartKind = 'Average';
            recommended = 100;
        }

        // making a chart
        $scope.makeChart = function() {

            var dateCheck;
            var beatCheck;
            var graphRowData = [];
            graphRowData.push(['Date', chartKind, 'Recommended']);

            if($scope.lastDate < $scope.beginDate){
                $scope.error = new Error("Start date must be smaller or equal to end date!(Click to remove message.)");
                return ;
            }

            // adding dates in range with heartbeat data
            for (i = 0; i < $scope.allBeats.length; i++) {

                if(chartKind == 'Maximum'){
                    beatCheck = $scope.allBeats[i].max;
                } else if(chartKind == 'Minimum'){
                    beatCheck = $scope.allBeats[i].min;
                } else {
                    beatCheck = $scope.allBeats[i].avg;
                }
                dateCheck = $scope.allBeats[i].date.split('T')[0];

                if (dateCheck >= $scope.beginDate && dateCheck <= $scope.lastDate){
                    graphRowData.push([String(dateCheck), beatCheck, recommended]);
                }
            }

            // setting up GoogleCharts
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            // drawing chart
            function drawChart() {
                var data = google.visualization.arrayToDataTable(graphRowData);
                var options = {
                    title: 'Heartbeats',
                    curveType: 'function',
                    legend: { position: 'right'},
                    vAxis: {
                        title :'Beats per Min',
                        ticks: [40, 60, 80, 100, 120, 140, 160, 180, 200]
                    },
                    hAxis: {
                        title :'Date'
                    }
                };

                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
                chart.draw(data, options);
            }
        };
}]);