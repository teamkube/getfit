/*
 *
 * Author : Prasanna Shiwakoti
 * Modified : 12/13/2016
 *
 * Class :
 *      User
 *
 * Attributes :
 *      username -> username
 *      password -> password
 *
 *  Methods :
 *      $scope.register ->  takes the user's username and password from input form the Register view and passes it to
 *                          auth service, if there is no error go to welcome page
 *
 *      $scope.login -> takes the user's username and password from input form the Login view and passes it to
 *                      auth service, if there is no error go to welcome page
 */

app.controller('AuthController',[
    '$scope', '$state', 'auth',
    function($scope, $state, auth){
        $scope.user = {};
        
        $scope.register = function (){
            auth.register($scope.user).error(function(error){
                $scope.error = error;
            }).then(function(){
                $state.go('welcome');    
            });   
        };
        
        $scope.logIn = function (){
            auth.logIn($scope.user).error(function(error){
                $scope.error = error;
            }).then(function(){
                $state.go('welcome');    
            });   
        };
    
}]);