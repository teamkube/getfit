/*
 *
 * Author : Nick Welch
 * Modified : 12/13/2016
 *
 * Class :
 *      Step
 *
 * Attributes :
 *      date -> date
 *      step -> number of steps
 *      user -> user's ID
 *
 *  Methods : *
 *      $scope.addStep -> takes the user's steps and passes the Step object with user's id to createStep in
 *                          stepService
 *
 *      $scope.deleteOneStep -> deletes a Step's row by passing the Step object to deleteStep in stepService
 *          params:
 *              walk -> Step Object
 *
 *      $scope.startStepEdit -> starts Step row's edit
 *          params:
 *              walk -> Step Object
 *
 *      $scope.stopStepEdit -> stops Step row's edit
 *          params:
 *              walk -> Step Object
 *
 *      $scope.sendStepEdit -> takes the user's min, max, and avg and if any of it has been changed passes
 *                              the Step object with user's id to updateBeat in heartbeatService
 *          params:
 *              walk -> Step Object
 *              date -> Step Object' date
 *              steps -> Step Object's steps
 *
 *      $scope.dateSort -> set $scope.sortKind to Step.date
 *
 *      $scope.stepSort -> set $scope.sortKind to Step.step
 *
 *      $scope.remMes -> set $scope.error to an empty string
 *
 *      $scope.genChart -> makes the chart using Google Charts library and drawChart method
 *
 */

app.controller('StepController', [
    '$scope', 'allSteps','auth',
    function($scope, allSteps, auth){
        $scope.allSteps = allSteps.allSteps;
        $scope.profile = auth.currentUserId();
        $scope.date = new Date();
        $scope.maxDate = new Date();

        // variables used for search function
        $scope.sortKind = 'date';
        $scope.sortStep = '';
        $scope.sortRev = false;

        // variables used for graph function
        $scope.startDate = '';
        $scope.endDate = '';

        // creating steps
        $scope.addStep = function () {
            if(!$scope.date || $scope.date === '' || !$scope.step || $scope.step === ''){
                return ;
            };

            if($scope.step < 1){
                $scope.error = new Error("Enter a positive number for Steps!(Click to remove message.)");
                return ;
            };

            $scope.date.setHours(0,0,0,0);

            allSteps.createStep({
                date: $scope.date,
                step: $scope.step,
                user: $scope.profile
            }, auth.currentUserId())
                .error(function(error){
                $scope.error = error;
            });

            $scope.step = '';
            $scope.editing = 'false';
        };

        // deleting steps
        $scope.deleteOneStep = function (walk) {
            allSteps.deleteStep(walk, auth.currentUserId());
        };

        // starting steps edit
        $scope.startStepEdit = function (walk) {
            walk.editing = true;
        };

        // stopping steps edit
        $scope.stopStepEdit = function (walk) {
            walk.editing = false;
        };

        // sending steps edit
        $scope.sendStepEdit = function (walk, date, steps) {
            walk.editing = false;
            var tempDate = walk.date;
            var tempStep = walk.step;
            ;

            if(steps < 1){
                $scope.error = new Error("Enter a positive number for Steps!(Click to remove message.)");
                return ;
            };

            walk.date = date;
            walk.step = steps

            allSteps.updateStep(walk, auth.currentUserId())
                .error(function(error){
                    $scope.error = error;
                    walk.date = tempDate;
                    walk.step = tempStep;
                });
        };

        // sorting by desired value
        $scope.dateSort = function(){
                $scope.sortRev = (!($scope.sortRev));
                $scope.sortKind = 'date';
        };

        $scope.stepSort = function(){
                $scope.sortRev = (!($scope.sortRev));
                $scope.sortKind = 'step';
        };

        $scope.remMes = function () {
            $scope.error = "";
        };

        // generating steps chart
		$scope.genChart = function() {
            var recommended = 950;
			var dateCheck;
            var graphRowData = [];
            graphRowData.push(['Date', 'Steps', 'Recommened']);

            if($scope.endDate < $scope.startDate){
                $scope.error = new Error("Start date must be smaller or equal to end date!(Click to remove message.)");
                return ;
            }

            // adding dates in range with heartbeat data
			for (i = 0; i < $scope.allSteps.length; i++) {
				dateCheck = $scope.allSteps[i].date.split('T')[0];

				if (dateCheck >= $scope.startDate && dateCheck <= $scope.endDate){
					graphRowData.push([String(dateCheck), $scope.allSteps[i].step, recommended]);
                }
			}

            // setting up GoogleCharts
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            // drawing chart
            function drawChart() {
                var data = google.visualization.arrayToDataTable(graphRowData);
                var options = {
                    title: 'Steps',
                    curveType: 'function',
                    legend: { position: 'right'},
                    vAxis: {
                        title :'# of Steps',

                    },
                    hAxis: {
                        title :'Date'
                    }
                };

                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
                chart.draw(data, options);
            };
		};


}]);