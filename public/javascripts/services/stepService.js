app.factory('allSteps', ['$http',  function($http){
    var allStepsData = {
        allSteps: []
    };

    // getting steps data from database for the user
    allStepsData.getAll = function (id) {
        return $http.get('user/' + id +'/run').success(function (data) {
            angular.copy(data, allStepsData.allSteps);
        });
    };

    // creating step
    allStepsData.createStep = function (walk, id) {
        return $http.post('user/' + id +'/run', walk).success(function(data) {
            allStepsData.allSteps.push(data);
        });
    };

    // deleting steps
    allStepsData.deleteStep = function(walk, id){
        return $http.delete('user/' + id + '/run/' + walk._id).success(function (data) {
            angular.copy(data, allStepsData.allSteps);
        });
    };

    // updating steps
    allStepsData.updateStep = function (walk, id) {
        return $http.put('user/' + id + '/run/' + walk._id, walk).success(function(data) {
        });
    };

    return allStepsData;
}]);

