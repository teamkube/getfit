app.factory('auth', ['$http', '$window', function($http, $window){
    var auth = {};    
    
    // saving token to keep user logged in
    auth.saveToken = function (token){
        $window.localStorage['get-fit-token'] = token;
    };
    
    // getting token to check user status
    auth.getToken = function (){
        return $window.localStorage['get-fit-token'];
    }    
    
    // check to see if user is logged in
    auth.isLoggedIn = function(){
        var token = auth.getToken();

        if(token){
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload.exp > Date.now() / 1000;
        } else {
            return false;
        } 
    };
    
    // get current user info
    auth.currentUser = function (){
        if(auth.isLoggedIn()){
            var token = auth.getToken();
            var payload = JSON.parse($window.atob(token.split('.')[1]));
            
            return payload.username;
        }
    };

    // get current user info
    auth.currentUserId = function (){
        if(auth.isLoggedIn()){
            var token = auth.getToken();
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload._id;
        }
    };

    // saving a new usre
    auth.register = function (user){
        return $http.post('/register', user).success(function(data){
            auth.saveToken(data.token);
        });
    };
    
    // checking database for login info
    auth.logIn = function (user){
        return $http.post('/login', user).success(function(data){
            auth.saveToken(data.token);
        });
    };
    
    // removing token while logging out
    auth.logOut = function(){
        $window.localStorage.removeItem('get-fit-token');
    };
    
    return auth;
}]);