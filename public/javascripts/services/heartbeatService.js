app.factory('allBeats', ['$http',  function($http){
    var allBeatsData = {
        allBeats: []
    };

    // getting heartbeat data from database for the user
    allBeatsData.getAll = function (id) {
        return $http.get('user/' + id +'/heartbeat').success(function (data) {
            angular.copy(data, allBeatsData.allBeats);
        });
    };

    // creating heartbeat
    allBeatsData.createBeat = function (beat, id) {
        return $http.post('user/' + id +'/heartbeat', beat).success(function(data) {
            allBeatsData.allBeats.push(data);
        });
    };

    // deleting heartbeat
    allBeatsData.deleteBeat = function(beat, id){
        return $http.delete('user/' + id + '/heartbeat/' + beat._id).success(function (data) {
            angular.copy(data, allBeatsData.allBeats);
        });
    };

    //updating heartbeat
    allBeatsData.updateBeat = function (beat, id) {
        return $http.put('user/' + id + '/heartbeat/' + beat._id, beat).success(function(data) {
        });
    };

    return  allBeatsData;
}]);