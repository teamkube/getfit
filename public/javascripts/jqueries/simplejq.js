var main = function() { 
    
    // getting window url
    var url = window.location.href;
    // chopping the string and turning into an id    
    var result = "#" + url.split("/#/")[1];
    // when window is refreshed set the result as active    
    $(window).on("load", function(){
        $(result).addClass("active");
    });    
    
    // when a menu item is clicked switch tab to active
    $("#list li").click(function() {
        $("#list li").removeClass("active");
        $(this).addClass("active");
    });
};
$(document).ready(main);