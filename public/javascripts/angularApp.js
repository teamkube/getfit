var app = angular.module('getfit', ['ui.router','ngMaterial', 'ngMessages']);

app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider,$urlRouterProvider){
        // check state and go to appropriate page script
        $stateProvider
            .state('home',{
                url:'/home',
                templateUrl: '/home.html',
                controller: 'MainController'
            })
            .state('welcome',{
                url:'/welcome',
                templateUrl: '/welcome.html',
                controller: 'NavController'
            })
            .state('goodbye',{
                url:'/goodbye',
                templateUrl: '/goodbye.html',
                controller: 'NavController'
            })
            .state('steps',{
                url:'/steps',
                templateUrl: '/steps.html',
                controller: 'StepController',
                resolve: {
                    postPromise: ['allSteps','auth', function(allSteps, auth){
                        return allSteps.getAll(auth.currentUserId());
                    }]
                }
            })
            .state('heartbeats',{
                url:'/heartbeats',
                templateUrl: '/heartbeats.html',
                controller: 'HeartbeatController',
                resolve: {
                    postPromise: ['allBeats','auth', function(allBeats, auth){
                        return allBeats.getAll(auth.currentUserId());
                    }]
                }
            })
            .state('register',{
                url:'/register',
                templateUrl: '/register.html',
                controller: 'AuthController',
                onEnter: ['$state', 'auth', function($state, auth){
                    if(auth.isLoggedIn()){
                        $state.go('welcome')
                    }
                }]
            })
            .state('login',{
                url:'/login',
                templateUrl: '/login.html',
                controller: 'AuthController',
                onEnter: ['$state', 'auth',
                    function($state, auth){
                        if(auth.isLoggedIn()){
                            $state.go('welcome')
                        }
                    }]
            })

        // if lost, go home
        $urlRouterProvider.otherwise('home');

    }]);




