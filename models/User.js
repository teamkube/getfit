var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

//make user schema with username and password
var UserSchema = new mongoose.Schema({
    username: {type: String, lowercase: true, unique: true},
    hash: String,
    salt: String,
    stepz: [{type: mongoose.Schema.Types.ObjectId, ref: 'Step'}],
    beatz: [{type: mongoose.Schema.Types.ObjectId, ref: 'Beat'}]
});

// setPassword() method that accepts a password
// then generates a salt and hash
UserSchema.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    //pbkdf2Sync() params: password, salt, iterations, key length
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

// validPassword() method that checks the password
UserSchema.methods.validPassword = function(password){
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
    return this.hash == hash;
};

// generateJWT() generating JWT tokens for session management
UserSchema.methods.generateJWT = function() {
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(today.getDate() + 60);
    
    return jwt.sign({
        _id: this.id,
        username: this.username,
        exp: parseInt(exp.getTime() / 1000),
    }, 'STRONG');    
};

mongoose.model('User',UserSchema);