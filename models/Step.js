var mongoose = require('mongoose');

// Creating Step Schema to hold steps data
var StepSchema = new mongoose.Schema({
    date: Date,
    step: Number,
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

// making a primary index with a composite key of (user, date)
StepSchema.index({user: 1, date: -1}, {unique: true});

mongoose.model('Step', StepSchema);