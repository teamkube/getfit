var mongoose = require('mongoose');

// Creating Beat Schema to hold heartbeat data
var BeatSchema = new mongoose.Schema({
    date:  Date,
    max: Number,
    min: Number,
    avg: Number,
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

// making a primary index with a composite key of (user, date)
BeatSchema.index({user: 1, date: -1}, {unique: true});

mongoose.model('Beat', BeatSchema);