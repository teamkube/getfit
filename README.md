# Get Fit #

## Instructions
`npm run start` will start the app but you must have a valid mongo db address in line 11 of app.js. ` mongoose.connect('mongodb://<add location of db here>')`.

## Class descriptions
Detailed infromation about each controllers methods can be found at `getFit / public / javascripts / controllers /` , just click on the controller you desire to learn more about.

## things to do / bugs
### front end
* steps must be > 0 -- done
* heartbeats must be in range, min < avg < max -- done
* allow one entry per date -- done
* sorting, paging -- done
